﻿#include "Start.h"

#include "Engine/Image.h"

//コンストラクタ
Start::Start(GameObject * parent)
	: GameObject(parent, "Start")
{
}

//初期化
void Start::Initialize()
{
	startImageHandle_ = Image::Load("Image/start.png");
	SetPosition(XMVectorSet(0.0f, 0.25f, 0.f, 0.f));
}

//更新
void Start::Update()
{
}

//描画
void Start::Draw()
{
	Image::SetTransform(startImageHandle_, transform_);
	Image::Draw(startImageHandle_);
}

//開放
void Start::Release()
{
	startImageHandle_ = -1;
}
