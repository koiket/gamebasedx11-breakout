﻿#pragma once
#include "Engine/GameObject.h"

class Ball : public GameObject
{
public:
	//コンストラクタ
	Ball(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	// 跳ね返り判定
	static bool	IntersecsRectVsCircle(XMVECTOR rectPosition, XMVECTOR rectExtent, XMVECTOR circlePosition, float radius);
	static bool IntersecsLineSegmentVsLineSegment2D(XMVECTOR& position, XMVECTOR p1, XMVECTOR v1, XMVECTOR p2, XMVECTOR v2);
	static XMVECTOR	Reflect2D(XMVECTOR vector, XMVECTOR normal);

	bool	AttemptBounceRect(const XMVECTOR rectPosition, const XMVECTOR rectExtent);
	void	ResetPosition();

protected:
	int		ballImageHandle_ = -1;
	XMVECTOR	moveDirection_ = g_XMZero;
	float	radius_ = 0.f;
	bool	isInterSects_ = false;
	XMVECTOR	prevScreenPosition_ = g_XMZero;
};
