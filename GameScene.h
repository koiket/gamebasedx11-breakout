﻿#pragma once

#include "Engine/GameObject.h"

class GameScene : public GameObject
{
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	GameScene(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	// イメージ・スクリーン座標変換
	static XMVECTOR GetScreenSize();
	static XMVECTOR ImageToScreenPosition(XMVECTOR imagePosition);
	static XMVECTOR ScreenToImagePosition(XMVECTOR screenPosition);

	void OnGameStart();	// ゲームスタート時・再スタート時に呼ばれる
	void OnGamePlay();	// ゲームプレイ時に呼ばれる
	void OnGameOver();	// ゲームオーバー時に呼ばれる
	void OnChangeTitleScene();	// タイトルへ戻る時に呼ばれる
	void OnBlockDestory();	// ブロック消滅時に呼ばれる
	void OnMiss();	// ミスしたときに呼ばれる


protected:
	enum STATE {
		STATE_NONE,
		STATE_GAMESTART,	// 開始
		STATE_GAMEPLAY,		// プレイ
		STATE_GAMEOVER,		// 終了
	};
	STATE state_ = STATE_NONE;
	void WaitAndCall(float waitSeconds, void (GameScene::*callbackFunction)());
	void (GameScene::*callbackFunction_)()	= nullptr;
	float waitSeconds_	= 0.f;
	size_t	leftBlocks_ = 0;	// 残りのブロック数

};