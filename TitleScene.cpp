#include "TitleScene.h"

#include "Engine/Image.h"
#include "Engine/SceneTimer.h"
#include "Engine/Input.h"
#include "Engine/SceneManager.h"

//コンストラクタ
TitleScene::TitleScene(GameObject * parent)
	: GameObject(parent, "TitleScene")
{
}

//初期化
void TitleScene::Initialize()
{
	titleImageHandle_ = Image::Load("Image/breakout.png");
	auto titleTransform = Transform();
	titleTransform.position_ = XMVectorSet(0.0f, 0.25f, 0.f, 0.f);
	Image::SetTransform(titleImageHandle_, titleTransform);
	pressSpaceImageHandle_ = Image::Load("Image/pressspacebar.png");
	auto pressSpaceTransform = Transform();
	pressSpaceTransform.position_ = XMVectorSet(0.0f, -0.25f, 0.f, 0.f);
	Image::SetTransform(pressSpaceImageHandle_, pressSpaceTransform);
}

//更新
void TitleScene::Update()
{
	if (Input::IsKeyDown(DIK_SPACE)) {
		auto sceneManager = static_cast<SceneManager*>(FindObject("SceneManager"));
		assert(sceneManager);
		sceneManager->ChangeScene(SCENE_ID_GAME);
	}
}

//描画
void TitleScene::Draw()
{
	Image::Draw(titleImageHandle_);

	auto pressSpaceAlpha = 1.f - fmod(SceneTimer::GetElapsedSecounds() * 0.75f, 1.f);
	Image::SetAlpha(pressSpaceImageHandle_, (int)(pressSpaceAlpha * 255.f));
	Image::Draw(pressSpaceImageHandle_);
}

//開放
void TitleScene::Release()
{
	titleImageHandle_ = -1;
	pressSpaceImageHandle_ = -1;
}
