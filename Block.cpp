﻿#include "Block.h"

#include "Engine/Image.h"
#include "GameScene.h"
#include "Ball.h"

//コンストラクタ
Block::Block(GameObject * parent)
	: GameObject(parent, "Block")
{
}

//初期化
void Block::Initialize()
{
	blockImageHandle_ = Image::Load("Image/block.png");
}

//更新
void Block::Update()
{
	XMVECTOR screenPosition = GameScene::ImageToScreenPosition(transform_.position_);	// スクリーン座標
	XMVECTOR blockeSize = XMLoadUInt2(&Image::GetSize(blockImageHandle_));

	// ポールと当たっているか確認
	auto ball = static_cast<Ball*>(FindObject("Ball"));
	if (ball) {
		bool isContact = ball->AttemptBounceRect(screenPosition, blockeSize * XMVectorReplicate(0.5f));
		if (isContact)
			KillMe();
	}
}

//描画
void Block::Draw()
{
	Image::SetTransform(blockImageHandle_, transform_);
	Image::Draw(blockImageHandle_);
}

//開放
void Block::Release()
{
	// 消滅したら GameScene へ通知する
	auto gameScene = static_cast<GameScene*>(FindObject("GameScene"));
	assert(gameScene);
	gameScene->OnBlockDestory();

	blockImageHandle_ = -1;
}
