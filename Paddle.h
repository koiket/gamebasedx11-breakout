﻿#pragma once
#include "Engine/GameObject.h"

class Paddle : public GameObject
{
public:
	//コンストラクタ
	Paddle(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

protected:
	int	padleImageHandle_ = -1;
};
