﻿#include "Ball.h"

#include "Engine/Image.h"
#include "Engine/Input.h"
#include "Engine/SceneTimer.h"
#include "GameScene.h"

//コンストラクタ
Ball::Ball(GameObject * parent)
	: GameObject(parent, "Ball")
{
}

//初期化
void Ball::Initialize()
{
	ballImageHandle_ = Image::Load("Image/ball.png");

	// 初期位置へ
	ResetPosition();

	moveDirection_ = XMVector2Normalize(XMVectorSet(1.f, 1.f, 0.f, 0.f));

	radius_ = (float)Image::GetSize(ballImageHandle_).x * 0.5f;	// 横幅の半分を半径とみなす
}

//更新
void Ball::Update()
{
	// ローカル座標系からスクリーン座標への変換 ただし、スクリーン座標系は中心が0でプラスは上方向
	XMVECTOR imagePosition = GetPosition();
	XMVECTOR screenPosition = GameScene::ImageToScreenPosition(imagePosition);	// スクリーン座標
	prevScreenPosition_ = screenPosition;
	// 移動
	float MOVE_SPEED = 750.f;	// pix/sec
	screenPosition += moveDirection_ * MOVE_SPEED * SceneTimer::GetDelta();

	// 移動範囲
	XMVECTOR screenSize = GameScene::GetScreenSize();
	XMVECTOR ballSize = XMLoadUInt2(&Image::GetSize(ballImageHandle_));
	XMVECTOR extentVector = (screenSize - ballSize) * 0.5f;	// -extentVector ~ extentVector

	// 壁の反射
	XMVECTOR reverseSwitchValue = XMVectorReplicate(1.f);	// g_XMOne
	XMVECTOR minDiff = screenPosition + extentVector;		// screenPosition - -extentVector
	XMVECTOR maxDiff = screenPosition - extentVector;

	XMVECTOR XMVECTOR_ZERO = XMVectorZero();					// g_XMZero
	XMVECTOR XMVECTOR_ONE = XMVectorReplicate(1.f);				// g_XMOne
	XMVECTOR XMVECTOR_NEGONE = XMVECTOR_ZERO - XMVECTOR_ONE;	// g_XMNegativeOne

	
	XMVECTOR minSwitch = XMVectorLess(minDiff, XMVECTOR_ZERO);		// マイナス方向の壁判定
	XMVECTOR maxSwitch = XMVectorGreater(maxDiff, XMVECTOR_ZERO);	// プラス方向の壁判定
	screenPosition -= XMVectorSelect(XMVECTOR_ZERO, minDiff, minSwitch);
	screenPosition -= XMVectorSelect(XMVECTOR_ZERO, maxDiff, maxSwitch);
	reverseSwitchValue *= XMVectorSelect(XMVECTOR_ONE, XMVECTOR_NEGONE, minSwitch);
	reverseSwitchValue *= XMVectorSelect(XMVECTOR_ONE, XMVECTOR_NEGONE, maxSwitch);

	moveDirection_ *= reverseSwitchValue;

	// スクリーン座標からローカル座標へ変換
	imagePosition = GameScene::ScreenToImagePosition(screenPosition);
	SetPosition(imagePosition);

	// 下の壁の反射を検知したらミスしたことにする
	if (XMVectorGetY(minSwitch) != 0.f) {
		auto gameScene = static_cast<GameScene*>(FindObject("GameScene"));
		assert(gameScene);
		gameScene->OnMiss();
	}
}

//描画
void Ball::Draw()
{
	Image::SetTransform(ballImageHandle_, transform_);
	Image::SetAlpha(ballImageHandle_, isInterSects_ ? 32 : 255);
	Image::Draw(ballImageHandle_);
}

//開放
void Ball::Release()
{
	ballImageHandle_ = -1;
}

bool Ball::IntersecsRectVsCircle(XMVECTOR rectPosition, XMVECTOR rectExtent, XMVECTOR circlePosition, float radius)
{
	// Rectangle vs Circle
	XMVECTOR	circleRadius = XMVectorReplicate(radius);
	circleRadius = XMVectorSelect(XMVectorZero(), circleRadius, g_XMSelect1100);	// Mask
	XMVECTOR	circleDistance = XMVectorAbs(circlePosition - rectPosition);

	uint32_t	noInterSects = XMVector2GreaterR(circleDistance, rectExtent + circleRadius);
	if (XMComparisonAnyTrue(noInterSects))
		return false;
	uint32_t	interSects = XMVector2GreaterOrEqualR(rectExtent, circleDistance);
	if (XMComparisonAnyTrue(interSects))
		return true;

	XMVECTOR	cornerDistanceTemp = circleDistance - rectExtent;
	float	cornerDistanceSq = XMVectorGetX(XMVector2Dot(cornerDistanceTemp, cornerDistanceTemp));
	return (cornerDistanceSq <= radius * radius);
}

// 線分と線分の交点を求める
bool Ball::IntersecsLineSegmentVsLineSegment2D(XMVECTOR& position, XMVECTOR p1, XMVECTOR v1, XMVECTOR p2, XMVECTOR v2)
{
	XMVECTOR det = XMVector2Cross(v1, v2);
	if (XMVectorGetX(det) == 0.f)
		return false;	// 平行

	XMVECTOR s = XMVectorReciprocal(det) * XMVector2Cross(v1, p1 - p2);
	position = v2 * s + p2;
	position = XMVectorSelect(XMVectorZero(), position, g_XMSelect1100);

	XMVECTOR det2 = XMVector2Cross(v2, v1);
	XMVECTOR s2 = XMVectorReciprocal(det2) * XMVector2Cross(v2, p2 - p1);

	XMVECTOR half = g_XMOneHalf;
	s -= half;
	s2 -= half;
	return XMVector2InBounds(s, half) && XMVector2InBounds(s2, half);
}

// 反射ベクトルを求める
XMVECTOR Ball::Reflect2D(XMVECTOR vector, XMVECTOR normal)
{
	XMVECTOR reflectVector = vector - normal * XMVector2Dot(vector, normal) * XMVectorReplicate(2.f);
	return XMVectorSelect(XMVectorZero(), reflectVector, g_XMSelect1100);
}

bool Ball::AttemptBounceRect(const XMVECTOR rectPosition, const XMVECTOR rectExtent)
{
	XMVECTOR imagePosition = GameScene::ImageToScreenPosition(GetPosition());
	isInterSects_ = IntersecsRectVsCircle(rectPosition, rectExtent, imagePosition, radius_);
	// 矩形と衝突していたら跳ね返る
	if (isInterSects_) {
		// rectにすべてが含まる場合は無視
		if (IntersecsRectVsCircle(rectPosition, rectExtent, prevScreenPosition_, radius_))
			return false;

		XMVECTOR radius = XMVectorReplicate(radius_);
		XMVECTOR extent = rectExtent + radius;
		XMVECTOR origin = rectPosition;

		XMVECTORF32 extentSign[4] = {
			{-1.f,  1.f, 0.f, 0.f},
			{ 1.f,  1.f, 0.f, 0.f},
			{ 1.f, -1.f, 0.f, 0.f},
			{-1.f, -1.f, 0.f, 0.f}
		};
		XMVECTOR edgePosition[4] = {
			origin + extent * extentSign[0],
			origin + extent * extentSign[1],
			origin + extent * extentSign[2],
			origin + extent * extentSign[3]
		};
		XMVECTOR edgeVector[4] = {
			edgePosition[1] - edgePosition[0],
			edgePosition[2] - edgePosition[1],
			edgePosition[3] - edgePosition[2],
			edgePosition[0] - edgePosition[3],
		};
		XMVECTORF32 edgeNormal[4] = {
			{ 0.f,  1.f, 0.f, 0.f},
			{ 1.f,  0.f, 0.f, 0.f},
			{ 0.f, -1.f, 0.f, 0.f},
			{-1.f,  0.f, 0.f, 0.f}
		};
		XMVECTOR ballVector = imagePosition - prevScreenPosition_;
		for (int i = 0; i < 4; i++) {
			XMVECTOR crossPosition = XMVectorZero();
			bool isInterSects = IntersecsLineSegmentVsLineSegment2D(crossPosition, prevScreenPosition_, ballVector, edgePosition[i], edgeVector[i]);
			if (isInterSects) {
				XMVECTOR moveDirection = Reflect2D(moveDirection_, edgeNormal[i]);	// 反射ベクトルを更新
				moveDirection_ = moveDirection;
				XMVECTOR fullDistance = XMVector2Length(ballVector);
				XMVECTOR toCrossDistance = XMVector2Length(crossPosition - prevScreenPosition_);
				XMVECTOR newScreenPosition = crossPosition + moveDirection_ * (fullDistance - toCrossDistance);
				// 位置を更新
				imagePosition = GameScene::ScreenToImagePosition(newScreenPosition);
				SetPosition(imagePosition);
				break;
			}
		}
	}

	return isInterSects_;
}

void	Ball::ResetPosition()
{
	// 初期位置は適当に
	SetPosition(XMVectorSet(0.0f, -0.5f, 0.f, 0.f));
}
