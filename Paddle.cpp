﻿#include "Paddle.h"

#include "Engine/Image.h"
#include "Engine/Input.h"
#include "Engine/SceneTimer.h"
#include "GameScene.h"
#include "Ball.h"

//コンストラクタ
Paddle::Paddle(GameObject * parent)
	: GameObject(parent, "Paddle")
{
}

//初期化
void Paddle::Initialize()
{
	padleImageHandle_ = Image::Load("Image/paddle.png");

	// 初期位置は適当に
	SetPosition(XMVectorSet(0.0f, -0.75f, 0.f, 0.f));
}

//更新
void Paddle::Update()
{
	// ローカル座標系からスクリーン座標への変換 ただし、スクリーン座標系は中心が0でプラスは上方向
	XMVECTOR imagePosition = GetPosition();
	XMVECTOR screenPosition = GameScene::ImageToScreenPosition(imagePosition);	// スクリーン座標

	// 定数
	XMVECTOR	XMVECTOR_ZERO = XMVectorZero();			// g_XMZero
	XMVECTOR	XMVECTOR_ONE = XMVectorReplicate(1.f);	// g_XMOne

	// 入力情報から移動方向ベクトルを作成する
	XMVECTOR	selectPlus = XMVectorSelectControl(Input::IsKey(DIK_RIGHT), 0 /* Input::IsKey(DIK_UP) */, 0, 0);
	XMVECTOR	selectMinus = XMVectorSelectControl(Input::IsKey(DIK_LEFT), 0 /* Input::IsKey(DIK_DOWN) */, 0, 0);

	// 移動方向を作成する
	XMVECTOR	moveDirection = XMVECTOR_ZERO;
	moveDirection += XMVectorSelect(XMVECTOR_ZERO, XMVECTOR_ONE, selectPlus);
	moveDirection -= XMVectorSelect(XMVECTOR_ZERO, XMVECTOR_ONE, selectMinus);

	// 移動
	float MOVE_SPEED = 1000.f;	// 1000 pix/sec
	screenPosition += moveDirection * XMVectorReplicate(MOVE_SPEED * SceneTimer::GetDelta());

	// 移動範囲を制限する
	XMVECTOR screenSize = GameScene::GetScreenSize();
	XMVECTOR paddleSize = XMLoadUInt2(&Image::GetSize(padleImageHandle_));
	XMVECTOR extentVector = (screenSize - paddleSize) * XMVectorReplicate(0.5f);
	screenPosition = XMVectorClamp(screenPosition, XMVectorNegate(extentVector), extentVector);

	// スクリーン座標からローカル座標へ変換
	imagePosition = GameScene::ScreenToImagePosition(screenPosition);
	SetPosition(imagePosition);

	// ボールと当たっているか確認
	auto ball = static_cast<Ball*>(FindObject("Ball"));
	if (ball) {
		ball->AttemptBounceRect(screenPosition, paddleSize * 0.5f);
	}
}

//描画
void Paddle::Draw()
{
	Image::SetTransform(padleImageHandle_, transform_);
	Image::Draw(padleImageHandle_);
}

//開放
void Paddle::Release()
{
	padleImageHandle_ = -1;
}
