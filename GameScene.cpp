﻿#include "GameScene.h"

#include "Paddle.h"
#include "Ball.h"
#include "Block.h"
#include "Start.h"
#include "GameOver.h"
#include "Engine/Direct3D.h"
#include "Engine/SceneManager.h"
#include "Engine/SceneTimer.h"

//コンストラクタ
GameScene::GameScene(GameObject * parent)
	: GameObject(parent, "GameScene")
{
}

//初期化
void GameScene::Initialize()
{
	Instantiate<Paddle>(this);
	Instantiate<Ball>(this);

	// ブック作成
	auto blockNumberX = 12u;
	auto blockNumberY = 6u;
	XMVECTORF32	blockClusterCenter = { 0.f, 0.5f, 0.f, 0.f };
	XMVECTORF32 blockClusterSize = { 1.5f, 0.70f, 0.f, 0.f };
	XMVECTOR	blockNumberR = XMVectorSet(1.f / (float)(blockNumberX - 1), 1.f / (float)(blockNumberY - 1), 0.f, 0.f);

	XMVECTOR	blockStep = blockClusterSize * blockNumberR;	// ブロックを並べる間隔(距離)
	XMVECTOR	blockOffset = blockClusterSize * XMVectorReplicate(-0.5f) + blockClusterCenter;	// 中心からのオフセット
	for (auto y = 0u; y < blockNumberY; y++)
		for (auto x = 0u; x < blockNumberX; x++) {
			XMVECTOR stepNumber = XMVectorSet((float)x, (float)y, 0.f, 0.f);
			Block* ball = Instantiate<Block>(this);
			assert(ball);
			ball->SetPosition(blockStep * stepNumber + blockOffset);
		}

	// 残っているブロック数をセット
	leftBlocks_ = blockNumberY * blockNumberX;

	// スタートとゲームオーバー
	auto start = Instantiate<Start>(this);
	assert(start);
	auto gameOver = Instantiate<GameOver>(this);
	assert(gameOver);
	start->Invisible();
	gameOver->Invisible();

	OnGameStart();
}

//更新
void GameScene::Update()
{
	if (waitSeconds_ > 0.f) {
		waitSeconds_ -= SceneTimer::GetDelta();
		if (waitSeconds_ <= 0.f) {
			assert(callbackFunction_);
			(this->*callbackFunction_)();
			callbackFunction_ = nullptr;
		}
	}
}

//描画
void GameScene::Draw()
{
}

//開放
void GameScene::Release()
{
}

// イメージ・スクリーン座標変換
XMVECTOR GameScene::GetScreenSize()
{
	return XMVectorSet((float)Direct3D::screenWidth_, (float)Direct3D::screenHeight_, 0.f, 0.f);
}

XMVECTOR  GameScene::ImageToScreenPosition(XMVECTOR imagePosition)
{
	// ローカル座標系からスクリーン座標への変換 ただし、スクリーン座標系は中心が0でプラスは上方向
	return imagePosition * GetScreenSize() * XMVectorReplicate(0.5f);	// スクリーン座標
}

XMVECTOR GameScene::ScreenToImagePosition(XMVECTOR screenPosition)
{
	XMVECTOR rscreenSize = XMVectorReciprocal(GetScreenSize());	// スクリーンサイズの逆数
	rscreenSize = XMVectorSelect(XMVectorZero(), rscreenSize, g_XMSelect1100);	// 無限大になっているzwを0に g_XMMaskXY
	return screenPosition * rscreenSize * XMVectorReplicate(2.0f);
}

void GameScene::OnGameStart()
{
	assert(state_ != STATE_GAMESTART);
	state_ = STATE_GAMESTART;

	// パドルとボールの動きを停止
	auto ball = static_cast<Ball*>(FindObject("Ball"));
	assert(ball);
	auto paddle = static_cast<Paddle*>(FindObject("Paddle"));
	assert(paddle);
	ball->Leave();
	paddle->Leave();

	// ボールの位置をリセット
	ball->ResetPosition();

	// STARTを表示
	auto start = static_cast<Start*>(FindObject("Start"));
	assert(start);
	start->Visible();

	// 2秒まってから OnGamePlay を呼び出す
	WaitAndCall(2.f, &GameScene::OnGamePlay);
}

void GameScene::OnGamePlay()
{
	assert(state_ != STATE_GAMEPLAY);
	state_ = STATE_GAMEPLAY;

	auto ball = static_cast<Ball*>(FindObject("Ball"));
	assert(ball);
	auto paddle = static_cast<Paddle*>(FindObject("Paddle"));
	assert(paddle);
	// パドルとボールの動き再開
	ball->Enter();
	paddle->Enter();

	// STARTを非表示
	auto start = static_cast<Start*>(FindObject("Start"));
	assert(start);
	start->Invisible();
}

void GameScene::OnGameOver()
{
	assert(state_ != STATE_GAMEOVER);
	state_ = STATE_GAMEOVER;

	// パドルとボールの動きを停止
	auto ball = static_cast<Ball*>(FindObject("Ball"));
	assert(ball);
	auto paddle = static_cast<Paddle*>(FindObject("Paddle"));
	assert(paddle);
	ball->Leave();
	paddle->Leave();

	// GAMEOVERを表示
	auto gameOver = static_cast<Start*>(FindObject("GameOver"));
	assert(gameOver);
	gameOver->Visible();

	// 2秒まってから OnChangeTitleScene を呼び出す
	WaitAndCall(2.f, &GameScene::OnChangeTitleScene);
}

void GameScene::OnChangeTitleScene()
{
	auto sceneManager = static_cast<SceneManager*>(FindObject("SceneManager"));
	assert(sceneManager);
	sceneManager->ChangeScene(SCENE_ID_TITLE);
}

void GameScene::WaitAndCall(float waitSeconds, void (GameScene::*callbackFunction)())
{
	assert(waitSeconds > 0.f);
	assert(callbackFunction);

	callbackFunction_ = callbackFunction;
	waitSeconds_ = waitSeconds;
}

void GameScene::OnBlockDestory()
{
	// 残りのブロックが無くなったらゲームオーバー
	if (--leftBlocks_ <= 0 && state_ == STATE_GAMEPLAY)
		OnGameOver();
}

void GameScene::OnMiss()
{
	OnGameOver();
}
