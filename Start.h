﻿#pragma once
#include "Engine/GameObject.h"

class Start : public GameObject
{
public:
	//コンストラクタ
	Start(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

protected:
	int	startImageHandle_ = -1;
};