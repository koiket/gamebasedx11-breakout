﻿#include "GameOver.h"

#include "Engine/Image.h"

//コンストラクタ
GameOver::GameOver(GameObject * parent)
	: GameObject(parent, "GameOver")
{
}

//初期化
void GameOver::Initialize()
{
	gameOverImageHandle_ = Image::Load("Image/gameover.png");
	SetPosition(XMVectorSet(0.0f, 0.25f, 0.f, 0.f));
}

//更新
void GameOver::Update()
{
}

//描画
void GameOver::Draw()
{
	Image::SetTransform(gameOverImageHandle_, transform_);
	Image::Draw(gameOverImageHandle_);
}

//開放
void GameOver::Release()
{
	gameOverImageHandle_ = -1;
}
